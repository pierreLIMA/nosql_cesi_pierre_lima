/* mySeedScript.js */

// require the necessary libraries
const { faker } = require("@faker-js/faker");
const MongoClient = require("mongodb").MongoClient;



async function seedDB() {
    // Connection URL
    //const uri = "mongodb://root:root@localhost:27017";
    
    //mongodb+srv://pierreLIMA:Btpvrd4552@cluster0.uer6xa0.mongodb.net/test

    

    try {
        const client = await MongoClient.connect('mongodb://127.0.0.1:27017/twitter')
        console.log("Connected correctly to server");

        const dbusers = client.db("twitter").collection("users");
        const dbtweets = client.db("twitter").collection("tweets");
        const dbcomments = client.db("twitter").collection("comments");

        // The drop() command destroys all data from a collection.
        // Make sure you run it against proper database and collection.
        dbusers.drop();
        dbtweets.drop();
        dbcomments.drop();

        // make a bunch of time series data

        let users = [];
        let comments = [];
        let tweets = [];

        let nb_users = 1001
        for (let i = 0; i < nb_users; i++) {
            const firstName = faker.name.firstName();
            const lastName = faker.name.lastName();
            const username = faker.internet.userName()

            let list_id_tweets = [];
            
            for (let w=0; w< faker.datatype.number({ min: 0, max: 10, precision: 1 }); w++){
                
                let list_id_comments = [];
            

                for (let y=0; y< faker.datatype.number({ min: 0, max: 10, precision: 1 }); y++){
                    let comment = {
                        _id: "tweet_"+w+"_comment_"+y,
                        texte: faker.lorem.text(),
                        like: faker.datatype.number({ min: 0, max:  100000000, precision: 1 })
                    }
                    comments.push(comment);
                    list_id_comments.push("tweet_"+w+"_comment_"+y);
                }

                let tweet = {
                    _id: "user_"+i+"_tweet_"+w,
                    texte: faker.lorem.text(),
                    image: faker.animal.type(),
                    like: faker.datatype.number({ min: 0, max:  100000000, precision: 1 }),
                    commentaires: list_id_comments
                    
                }
                tweets.push(tweet);
                list_id_tweets.push("user_"+i+"_tweet_"+w);



            }


           
            let user = {_id: "user_"+i, firstName: firstName, lastName: lastName, username: username, tweets: list_id_tweets}
            

            users.push(user);
        }
        dbcomments.insertMany(comments);
        dbtweets.insertMany(tweets);
        dbusers.insertMany(users);

        console.log("Database seeded! :)");
    } catch (err) {
        console.log(err.stack);
    }
}

seedDB();