const { faker } = require("@faker-js/faker");
const MongoClient = require("mongodb").MongoClient;
const { uuid } = require('uuidv4');


async function seedDB() {

    const client = await MongoClient.connect('mongodb://127.0.0.1:27017/');
    console.log("Connected correctly to server");

    const collection_student = client.db("university").collection("Student");
    collection_student.drop();

    let students = [];
    let universities = [];
    let exams = [];
    let list_exams = [];
    for (let index_student = 0; index_student < 10; index_student++){
        let exam = {
            _id: uuid(),
            name: faker.lorem.word(),
        }
        list_exams.push(exam);
    }
    
    let nb_universities = 50;
    for(let index_universities = 0; index_universities < nb_universities; index_universities++){
        let address = {
            street: faker.address.street(),
            city: faker.address.city(),
            zip: faker.address.zipCode(),
            country: faker.address.country()
        }
        let university = {
            _id: uuid(),
            name: faker.company.name(),
            private: faker.datatype.boolean(),
            address: address
        }

        universities.push(university);
    }
    let nb_students = 1000;
    
    for(let index_student = 0; index_student < nb_students; index_student++){
        let list_exams_student = list_exams;
        for (let exam of list_exams_student) {
            exam.value =  faker.datatype.number({ min: 0, max: 20, precision: 1 });
        }

        let student = {
            _id: uuid(),
            firstname: faker.name.firstName(),
            lastname: faker.name.lastName(),
            gender: faker.name.sex(),
            age: faker.datatype.number({ min: 17, max: 30, precision: 1 }),
            university: universities[faker.datatype.number({ min: 0, max: 49, precision: 1 })],
            country: faker.address.country(),
            exams: list_exams_student,

        }
        students.push(student);
    }
    collection_student.insertMany(students);

    console.log("Database seeded! :)");
    
}

seedDB();
