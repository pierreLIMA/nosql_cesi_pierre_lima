// MongoDB Playground
use('university');

//1. Affichez la liste des universités.

db.Student.aggregate(
    {$group : {_id:"$university.name"}}
    
)

//2. Recherchez l'université la plus représentée dans la base de données (selon le nombre d'étudiants associés).

db.Student.aggregate([
    {$group : {_id:"$university.name", count:{$sum:1}}},
    {$sort:{"count":-1}},
    { $limit: 1}
    
])


 //3. Calculez la moyenne générale.
 
db.Student.aggregate(
    { $unwind: "$exams" },
    { $group: {_id: null, som_notes: {$sum: "$exams.value"}, nb_notes: {$sum: 1}}},
    { $project: { _id: 0, moyenne_notes: { $divide: ["$som_notes", "$nb_notes"] }}} 
    )

//4. Calculez l'âge moyen des étudiants.

db.Student.aggregate(
    { $group: {_id: null, som_ages: {$sum: "$age"}, nb_student: {$sum: 1}}},
    { $project: { _id: 0, moyenne_ages: { $divide: ["$som_ages", "$nb_student"] }}} 
    )

//5. Calculez le nombre d'étudiants par pays et classez les pays par nombre descendant d'étudiants.

db.Student.aggregate(
    { $group: {_id: "$country", nb_students: {$sum: 1}}},
    {$sort:{"nb_students":-1}}
    )
    